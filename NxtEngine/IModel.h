#pragma once
#include "../NxtEngine/IRenderer.h"

namespace nxt {

	enum class ModelTypes {
		TRIANGLE,
		QUAD,
		CUBE,
		SUBSET_MODEL
	};
	enum class ImporterType {
		ASSIMP,
		GLTF,
		NONE,
	};
	enum class VertexType {
		VERTEX_POS,
		VERTEX_POS_TEX,
		VERTEX_POS_TEX_NORM,
		VERTEX_POS_TEX_NORM_TANG_BITANG
	};
	struct StandardVertex {
		float position[4];
		float tex[2];
		float normal[3];
	};

	Model* CreateModel(CommandBuffer* commandBuffer, VertexType vertexType, ImporterType importerType, ModelTypes modelTypes, const char* modelName);
};