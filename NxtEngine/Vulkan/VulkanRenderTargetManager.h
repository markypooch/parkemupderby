#pragma once
#include <vulkan/vulkan.hpp>
#include "../RenderTargetManager.h"

namespace nxt {

	Texture* CreateTexture(TextureDesc textureDesc, bool depth, bool upload);
	struct RenderTarget : public IResource {
		Texture* texture[3];
		bool depth;
	};

	class VulkanRenderTargetManager : public IRenderTargetManager {
	public:
		virtual IResource* Load(RenderTargetDesc renderTargetDesc) {
			
			RenderTarget* renderTarget = new RenderTarget();
			for (int j = 0; j < 3; j++) {
				TextureDesc textureDesc{};
				textureDesc.width = renderTargetDesc.width;
				textureDesc.height = renderTargetDesc.height;
				textureDesc.depthOrArraySize = 1;
				textureDesc.format = (!renderTargetDesc.depth) ? renderTargetDesc.colorFormat : renderTargetDesc.depthFormat;
				textureDesc.sampleCount = 1;
				textureDesc.sampleQuality = 0;
				textureDesc.flags = (renderTargetDesc.depth) ? VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT : VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
				textureDesc.flags |= VK_IMAGE_USAGE_SAMPLED_BIT;

				renderTarget->texture[j] = CreateTexture(textureDesc, renderTargetDesc.depth, false);
			}
			renderTarget->depth = renderTargetDesc.depth;
			return renderTarget;
		}
	};
};