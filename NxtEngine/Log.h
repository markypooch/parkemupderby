#pragma once
#include <vector>

namespace nxt {

	enum class LogLevel {
		LOG_INFO,
		LOG_ERROR
	};

	class Log
	{
	public:
		static void AppendMessage(LogLevel logLevel, const char* message) {};
		static void Flush() {};
	private:
		static std::vector<const char*> logBuffer;
	};
};