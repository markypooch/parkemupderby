#ifdef _WIN32

#include "IOperatingSystem.h"
#include <Windows.h>
#include <WinUser.h>

#define WIN32_LEAN_AND_MEAN

namespace nxt {
	struct WindowHandle {
		HWND hWnd;
		HINSTANCE hInstance;
	};
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_QUIT:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

wchar_t* convertCharArrayToLPCWSTR(const char* charArray)
{
	wchar_t* wString = new wchar_t[4096];
	MultiByteToWideChar(CP_ACP, 0, charArray, -1, wString, 4096);
	return wString;
}

nxt::WindowHandle* nxt::InitWindow(const WindowDesc windowDesc) {

	HWND hWnd;
	WNDCLASSEX wnd{};
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.lpfnWndProc = WndProc;
	wnd.hInstance = NULL;
	wnd.lpszClassName = convertCharArrayToLPCWSTR(windowDesc.className);
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;

	if (RegisterClassEx(&wnd)) {

	}

	hWnd = CreateWindowEx(0, convertCharArrayToLPCWSTR(windowDesc.className), convertCharArrayToLPCWSTR(windowDesc.windowName), WS_OVERLAPPEDWINDOW, 0, 0, windowDesc.width, windowDesc.height, 0, 0, 0, 0);
	LONG hModule = GetWindowLongPtr(hWnd, GWLP_HINSTANCE);

	WindowHandle* windowHandle = new WindowHandle();
	windowHandle->hWnd = hWnd;
	windowHandle->hInstance = (HINSTANCE)hModule;

	return windowHandle;
}

void nxt::MainLoop(WindowHandle* windowHandle, void(*mainloop)()) {
	ShowWindow(windowHandle->hWnd, SW_RESTORE);

	MSG msg{};
	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, windowHandle->hWnd, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			mainloop();
		}
	}

}
#endif