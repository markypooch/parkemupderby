#include "RenderGraph.h"
#include "tinyjson.hpp"

#include <unordered_map>
#include <fstream>

using namespace nxt;
using namespace std;
using namespace tiny;

/*

        G
	  / E
     G





*/

ResourceState RenderGraph::ParseResourceState(std::string srcState) {
	ResourceState dstState = ResourceState::UNDEFINED;
	if (srcState == "DepthRead")
		dstState = ResourceState::DEPTH_READ;
	else if (srcState == "DepthWrite")
		dstState = ResourceState::DEPTH_WRITE;
	else if (srcState == "RenderTarget")
		dstState = ResourceState::RENDER_TARGET;
	else if (srcState == "Present")
		dstState = ResourceState::PRESENT;

	return dstState;
}

void RenderGraph::BuildGraph(xarray passes, RenderNode* renderNode) {

	RenderPassDesc renderPassDesc{};
	string passName = passes.Get<string>("Name");
	
	for (int j = 0; j < passes.Count(); j++) {
		passes.Enter(j);

		xarray resources = passes.Get<xarray>("Attachments");
		renderPassDesc.attachmentCount = resources.Count();
		for (int k = 0; k < resources.Count(); k++) {

			resources.Enter(k);

			string resourceName = resources.Get<string>("Name");

			RenderTargetDesc rtDesc{};
			rtDesc.height = resourcesMap[resourceName].height;
			rtDesc.width = resourcesMap[resourceName].width;
			rtDesc.dstStates = 	ParseResourceState(resources.Get<string>("DstResourceState"));
			if (resourcesMap[resourceName].depth) {
				rtDesc.depth = resourcesMap[resourceName].depth;
				rtDesc.depthFormat = resourcesMap[resourceName].depthFormat;
			}
			else {
				rtDesc.colorFormat = resourcesMap[resourceName].colorFormat;
			}

			renderPassDesc.rtDesc[k] = rtDesc;
		}

		renderPassDesc.clearColor[0] = 0.0f;
		renderPassDesc.clearColor[1] = 0.0f;
		renderPassDesc.clearColor[2] = 0.0f;
		renderPassDesc.clearColor[3] = 0.0f;

		CreateRenderPass(renderPassDesc, &renderNode->renderPass);

		//if (idx == -1)
		//	break; 

		//BuildGraph(passes, edge->renderNode, idx);
	}
}

RenderGraph::RenderGraph(const char* filePath) {
	ifstream myfile("C:\\Users\\mhans\\source\\repos\\ParkemupDerby\\Frame.json");

	std::string line;
	std::string jsonStr;
	if (myfile.is_open())
	{
		while (std::getline(myfile, line))
			jsonStr += line;
		myfile.close();
	}

	TinyJson json;
	json.ReadJson(jsonStr);
	
	xarray resources = json.Get<xarray>("Resources");
	for (int i = 0; i < resources.Count(); i++) {
		resources.Enter(i);

		string resourceType = resources.Get<string>("ResourceType");
		
		string rtName = resources.Get<string>("Name");
		string format = resources.Get<string>("Format");

		nxt::Format nxtFormat;
		if (format == "D32") {
			nxtFormat = nxt::Format::D32;
		}
		else if (format == "R8G8B8A8_UNORM") {
			nxtFormat = nxt::Format::R8G8B8A8_UNORM;
		}

		uint16_t width = resources.Get<uint16_t>("Width");
		uint16_t height = resources.Get<uint16_t>("Height");

		if (resourceType == "DepthTarget")
			resourcesMap[rtName].depth = 1;

		resourcesMap[rtName].name = rtName;
		resourcesMap[rtName].colorFormat = nxtFormat;
		resourcesMap[rtName].width = width;
		resourcesMap[rtName].height = height;
		resourcesMap[rtName].srcStates = ParseResourceState(resources.Get<string>("ResourceState"));
		

		//for (int ii = 0; ii < two.Count(); ii++) {
		//	two.Enter(ii);
		//	string love1 = two.Get<string>("love1");
		//	int love2 = two.Get<int>("love2");
		//	assert(love1 == "2233");
		//	assert(love2 == 44444);
		//}
	}

	renderNode = new RenderNode();
	xarray passes = json.Get<xarray>("passes");

	BuildGraph(passes, renderNode);
}
