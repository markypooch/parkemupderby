#pragma once
#include "../NxtEngine/NxtCommon.h"
#include <cstdint>

namespace nxt {

	struct WindowDesc {
		uint32_t width, height;
		const char* windowName;
		const char* className = "NXT";
	};

	WindowHandle*  InitWindow(const WindowDesc);
	void MainLoop(WindowHandle* windowHandle, void(*ptr)());
}