#pragma once
#include "NxtCommon.h"
#include "GraphicsTypes.h"
#include "RenderTargetManager.h"

#include <stdint.h>
#include <deque>
#include <string>

namespace nxt {
	struct RendererDesc {
		WindowHandle* pWindowHandle;
		float displayScale;

		uint16_t width, height;
		uint16_t refreshRate;
		uint8_t bufferCount;
		uint8_t window;
		uint8_t vsync;
		float clearColor[4];
	};
	struct MaterialDesc {
		int pTextureIndices[32];
		const char* textureNames[32];
		const char* materialName;
	};
	struct Material {
		RasterizerState rs;
		BlendState bs;
		DepthState ds;

		Shader* vs;
		Shader* ps;
	};
	struct Subset {
		unsigned int subsetIdxOffset = 0;
		Material material;
	};
	struct Model {
		VertexBuffer* vb;
		IndexBuffer *ib;
		Subset** subsets;
		Pipeline* pipeline;
		unsigned int numberOfSubset;
	};

	struct TextureDesc {
		uint32_t width, height;
		uint32_t mipCount;
		Format format;
		uint32_t sampleCount;
		uint32_t sampleQuality;
		uint32_t depthOrArraySize;
		int flags;
		bool depth, upload;
	};

	struct RenderPass;
	struct Attachment {
		RenderTarget* rt;
		ResourceState dstState;
	};
	struct RenderPassDesc {
		RenderPassType renderPassType;
		uint32_t attachmentCount;
		RenderTargetDesc rtDesc[8];
		LoadOperations loadOps[8];
		RenderTargetDesc depthDesc;

		//RenderPass input, output;
		float clearColor[4];
	};

	NxtResult InitializeRenderer(RendererDesc);

	RenderPass* GetPrimaryRenderPass();
	NxtResult CreateCommandBuffer(CommandBuffer**);
	Texture*  CreateTexture(TextureDesc textureDesc, bool depth, bool upload);

	IRenderTargetManager* GetRenderTargetManager();

	NxtResult CreateVertexBuffer(VertexBuffer**, uint32_t stride, uint32_t byteWidth);
	NxtResult CreateRenderPass(RenderPassDesc renderPassDesc, RenderPass**);
	NxtResult BeginRecording(CommandBuffer* cmdBuffer);

	NxtResult CmdCopyBufferData(CommandBuffer*, VertexBuffer*, void* pData, uint32_t stride, uint32_t byteWidth);
	NxtResult CmdCopyBufferData(CommandBuffer*, IndexBuffer*, void* pData, uint32_t stride, uint32_t byteWidth);
	//NxtResult CmdCopyBufferData(CommandBuffer*, VertexBuffer*, void* pData, uint32_t stride, uint32_t byteWidth);

	NxtResult CmdBeginRenderPass(CommandBuffer* cmdBuffer, RenderPass*);
	NxtResult CmdEndRenderPass(CommandBuffer* cmdBuffer, RenderPass*);

	NxtResult EndRecording(CommandBuffer* cmdBuffer);

	NxtResult SubmitCommandBuffers(uint8_t numCommandBuffers, CommandBuffer**);
	
};