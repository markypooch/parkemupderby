#pragma once
#include "IRenderer.h"
#include "IModel.h"
#include <vector>

#include "tinyjson.hpp"
#include <unordered_map>

namespace nxt {

	enum ResourceMoveType {
		COPY,
		MOVE
	};

	struct Attachment {
		RenderTarget* renderTarget;
		ResourceState srcState;
		ResourceState dstState;
	};

	class RenderNode {
	public:
		void AddModel();
		void Draw();
	private:
		friend class RenderGraph;

		std::vector<Model> models;
		RenderPass* renderPass;
	};

	class RenderGraph
	{
	public:
		RenderGraph(const char* data);
		~RenderGraph();
		
		void DrawGraph();
	private:
		void BuildGraph(tiny::xarray passOutput, RenderNode* renderNode);
		void TraceSubPasses(RenderPassDesc& renderPassDesc, tiny::xarray passOutput, RenderNode* renderNode, int index);

		std::unordered_map<std::string, RenderTargetDesc> resourcesMap;
		ResourceState ParseResourceState(std::string resourceState);
		ResourceState ParseSourceResourceState(std::string resourceState);
		RenderNode* renderNode;
	};
};