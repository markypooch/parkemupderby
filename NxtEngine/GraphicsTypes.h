#pragma once
#include <cstdint>

namespace nxt {
	enum class NxtResult {
		NXT_OK,
		NXT_ERROR
	};
	enum class Format {
		D32,
		R8G8B8A8_UNORM
	};
	enum class Usage {
		PIXEL_SHADER
	};
	enum class ResourceState {
		UNDEFINED,
		DEPTH_READ,
		DEPTH_WRITE,
		RENDER_TARGET,
		PRESENT
	};

	enum class RenderPassType {
		PRESENT,
		WRITE
	};
	enum class LoadOperations {
		CLEAR,
		LOAD,
		DONT_CARE
	};

	enum class BlendState {
	};
	enum class RasterizerState {
	};
	enum class DepthState {
	};


	struct Renderer;
	struct CommandBuffer;
	struct Texture;
	struct Buffer;
	struct VertexBuffer;
	struct IndexBuffer;
	struct ConstantBuffer;
	struct Shader;
	struct Pipeline;

	struct RenderTarget;
	struct ResourceBarrier {
		ResourceState prevState;
		ResourceState currState;
	};
};