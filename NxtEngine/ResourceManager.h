#pragma once
#include <unordered_map>
#include <functional>

namespace nxt {

    class IResource
    {
    public:
        ResourceState currState;
    };

    class ResourceManager
    {
        std::unordered_map<int, IResource*> managerResources;
    public:
        template <typename M, typename D>
        IResource* addResource(std::string name, M* resourceManager, D desc)
        {
            std::size_t str_hash = std::hash<std::string>{}(name);
            if (managerResources.count(str_hash) == 0) {
                IResource* iResource = resourceManager->Load(desc);
                managerResources[str_hash] = iResource;
                return iResource;
            }
            else {
                return managerResources[str_hash];
            }
        }
    };
};