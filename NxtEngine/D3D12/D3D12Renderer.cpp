#ifdef D3D12_API
#include "../../IRenderer.h"
#include "../../Log.h"

#include <d3d12.h>
#include <dxgi1_4.h>
#include <d3dcompiler.h>
#include "d3dx12.h"

#include <unordered_map>

namespace nxt {

	struct WindowHandle {
		HWND hWnd;
		HINSTANCE hInstance;
	};

	struct Renderer {
		ID3D12Device* device;
		ID3D12CommandQueue* primaryCmdQueue;
		ID3D12DescriptorHeap* srcRtvHeap;
		ID3D12DescriptorHeap* dstRtvHeap;
		ID3D12DescriptorHeap* srcDsvHeap;
		ID3D12DescriptorHeap* dstDsvHeap;
		ID3D12DescriptorHeap* primaryHeap;

		ID3D12Resource* primaryRtvTextures[128];
		
		unsigned int bufferCount;
		unsigned int rtvDescriptorOffset;
		unsigned int rtvHandlesAllocated;
		unsigned int dsvHandlesAllocated;

		IDXGISwapChain3* swapChain;
		uint8_t  currentFrameIndex;
		uint32_t width, height;

		D3D12_RECT scissorRect;
		D3D12_VIEWPORT viewPort;

		RenderPass* primaryRenderPass;
	} *pRenderer;

	struct CommandBuffer {
		ID3D12CommandAllocator* cmdAllocator[3];
		ID3D12GraphicsCommandList* cmdList;

		ID3D12Fence* fence[3];
		unsigned long long fenceValue[3];

		HANDLE fenceEvent;
		uint32_t frameIndex;
	};

	struct Texture {
		ID3D12Resource* pResourceUpload;
		ID3D12Resource* pResource;

		ResourceState currState;
	};

	struct Buffer {
		ID3D12Resource* pResourceUpload;
		ID3D12Resource* pResource;

		ResourceState currState;
		ResourceState dstState;
	};

	struct VertexBuffer {
		Buffer* buffer;
		D3D12_VERTEX_BUFFER_VIEW vbView;
	};

	struct Subpass {
		RenderTarget* pRenderTargets[8];
		RenderTarget* pDepthTarget;

		uint32_t attachmentCount;
		Model* models[1024];
	};

	struct RenderPass {
		uint32_t renderTargetCount;
		Attachment renderTargets[8];
		Attachment depthTarget;
		LoadOperations loadOperations[8];
		float clearColor[4];
	};
};

using namespace nxt;

D3D12_RESOURCE_STATES ConvertNxtResourceState(ResourceState resourceState) {
	switch (resourceState) {
	case ResourceState::RENDER_TARGET:
		return D3D12_RESOURCE_STATE_RENDER_TARGET;
	case ResourceState::PRESENT:
		return D3D12_RESOURCE_STATE_PRESENT;
	}
}

DXGI_FORMAT ConvertFormat(Format format) {
	switch (format) {
	case Format::R8G8B8A8_UNORM:
		return DXGI_FORMAT_R8G8B8A8_UNORM;
	case Format::D32:
		return DXGI_FORMAT_D32_FLOAT;
	}
}

Texture* CreateTexture(TextureDesc textureDesc, D3D12_CLEAR_VALUE d3d12ClearValue, bool upload) {

	D3D12_RESOURCE_DESC resourceDesc{};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.Alignment = 0;
	resourceDesc.DepthOrArraySize = textureDesc.depthOrArraySize;
	resourceDesc.Height = textureDesc.height;
	resourceDesc.Width = textureDesc.width;
	resourceDesc.MipLevels = textureDesc.mipCount;
	resourceDesc.Format = ConvertFormat(textureDesc.format);
	resourceDesc.SampleDesc.Count = textureDesc.sampleCount;
	resourceDesc.SampleDesc.Quality = textureDesc.sampleQuality;
	resourceDesc.Flags = (D3D12_RESOURCE_FLAGS)textureDesc.flags;

	Texture* texture = new Texture();
	D3D12_HEAP_PROPERTIES d3d12HeapProps{};
	if (upload) {
		D3D12_HEAP_PROPERTIES d3d12HeapProps{};
		d3d12HeapProps.Type = D3D12_HEAP_TYPE_UPLOAD;

		unsigned long long uploadBufferSize = 0;
		pRenderer->device->GetCopyableFootprints(&resourceDesc, 0, 1, 0, nullptr, nullptr, 0, &uploadBufferSize);

		D3D12_RESOURCE_DESC bufferDesc{};
		bufferDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		bufferDesc.Alignment = 0;
		bufferDesc.MipLevels = 1;
		bufferDesc.DepthOrArraySize = 1;
		bufferDesc.Width = uploadBufferSize;
		bufferDesc.Height = 1;
		bufferDesc.Format = DXGI_FORMAT_UNKNOWN;
		bufferDesc.SampleDesc.Count = 1;
		bufferDesc.SampleDesc.Quality = 0;
		bufferDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

		pRenderer->device->CreateCommittedResource(&d3d12HeapProps, D3D12_HEAP_FLAG_NONE, &bufferDesc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&texture->pResourceUpload));
	}

	d3d12HeapProps = {};
	d3d12HeapProps.Type = D3D12_HEAP_TYPE_DEFAULT;

	pRenderer->device->CreateCommittedResource(&d3d12HeapProps, D3D12_HEAP_FLAG_NONE, &resourceDesc, D3D12_RESOURCE_STATE_COPY_DEST, &d3d12ClearValue, IID_PPV_ARGS(&texture->pResource));

	return texture;
}

Buffer* CreateBuffer(uint32_t byteWidth) {

	Buffer* buffer = new Buffer();
	D3D12_RESOURCE_DESC bufferResource{};
	bufferResource.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	bufferResource.Alignment = 0;
	bufferResource.DepthOrArraySize = 0;
	bufferResource.Format = DXGI_FORMAT_UNKNOWN;
	bufferResource.Height = 1;
	bufferResource.Width = byteWidth;
	bufferResource.MipLevels = 1;
	bufferResource.SampleDesc.Count = 1;
	bufferResource.SampleDesc.Quality = 0;

	D3D12_HEAP_PROPERTIES props{};
	props.Type = D3D12_HEAP_TYPE_DEFAULT;

	pRenderer->device->CreateCommittedResource(&props, D3D12_HEAP_FLAG_NONE, &bufferResource, D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_PPV_ARGS(&buffer->pResource));

	props = {};
	props.Type = D3D12_HEAP_TYPE_UPLOAD;

	pRenderer->device->CreateCommittedResource(&props, D3D12_HEAP_FLAG_NONE, &bufferResource, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&buffer->pResourceUpload));

	return buffer;
}

NxtResult CopyDescriptor(int srcIndex, int dstIndex, ID3D12DescriptorHeap* srcHeap, ID3D12DescriptorHeap* dstHeap, D3D12_DESCRIPTOR_HEAP_TYPE type) {

	D3D12_CPU_DESCRIPTOR_HANDLE srcHandle = srcHeap->GetCPUDescriptorHandleForHeapStart();
	srcHandle.ptr = srcHandle.ptr + srcIndex * pRenderer->device->GetDescriptorHandleIncrementSize(type);

	D3D12_CPU_DESCRIPTOR_HANDLE dstHandle = dstHeap->GetCPUDescriptorHandleForHeapStart();
	dstHandle.ptr = dstHandle.ptr + dstIndex * pRenderer->device->GetDescriptorHandleIncrementSize(type);

	pRenderer->device->CopyDescriptorsSimple(1, dstHandle, srcHandle, type);

	return NxtResult::NXT_OK;
}

nxt::RenderPass* nxt::GetPrimaryRenderPass() {
	return pRenderer->primaryRenderPass;
}

NxtResult nxt::InitializeRenderer(RendererDesc rendererDesc) {

	Log::AppendMessage(LogLevel::LOG_INFO, "Initializing Renderer");

	pRenderer = new Renderer();
	pRenderer->bufferCount         = rendererDesc.bufferCount;

	IDXGIFactory1* factory;
	if (FAILED(CreateDXGIFactory1(IID_PPV_ARGS(&factory)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1000");
		return NxtResult::NXT_ERROR;
	}

	if (FAILED(D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_12_0, IID_PPV_ARGS(&pRenderer->device)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1001");
		return NxtResult::NXT_ERROR;
	}

	ID3D12Device* device = pRenderer->device;

	D3D12_COMMAND_QUEUE_DESC cmdQueueDesc{};
	cmdQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;

	if (FAILED(device->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&pRenderer->primaryCmdQueue)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1002");
		return NxtResult::NXT_ERROR;
	}
	
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc{};
	rtvHeapDesc.NumDescriptors = 1600;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;

	if (FAILED(device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&pRenderer->srcRtvHeap)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1003");
		return NxtResult::NXT_ERROR;
	}

	rtvHeapDesc = {};
	rtvHeapDesc.NumDescriptors = 8;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;

	if (FAILED(device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&pRenderer->dstRtvHeap)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1003");
		return NxtResult::NXT_ERROR;
	}

	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc{};
	dsvHeapDesc.NumDescriptors = 1600;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;

	if (FAILED(device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&pRenderer->srcDsvHeap)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1003");
		return NxtResult::NXT_ERROR;
	}

	dsvHeapDesc = {};
	dsvHeapDesc.NumDescriptors = 8;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;

	if (FAILED(device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&pRenderer->dstDsvHeap)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1003");
		return NxtResult::NXT_ERROR;
	}

	D3D12_DESCRIPTOR_HEAP_DESC cbvSrvUavDesc{};
	cbvSrvUavDesc.NumDescriptors = 32000;
	cbvSrvUavDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbvSrvUavDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	if (FAILED(pRenderer->device->CreateDescriptorHeap(&cbvSrvUavDesc, IID_PPV_ARGS(&pRenderer->primaryHeap)))) {
		return NxtResult::NXT_OK;
	}

	DXGI_SWAP_CHAIN_DESC swapDesc{};
	swapDesc.BufferCount = rendererDesc.bufferCount;
	swapDesc.BufferDesc.Width = rendererDesc.width;
	swapDesc.BufferDesc.Height = rendererDesc.height;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapDesc.BufferDesc.RefreshRate.Numerator = rendererDesc.refreshRate;
	swapDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapDesc.OutputWindow = (HWND)rendererDesc.pWindowHandle->hWnd;
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapDesc.SampleDesc.Count = 1;
	swapDesc.SampleDesc.Quality = 0;
	swapDesc.Windowed = rendererDesc.window;

	IDXGISwapChain* swapChain1;
	if (FAILED(factory->CreateSwapChain(pRenderer->primaryCmdQueue, &swapDesc, &swapChain1))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "1004");
		return NxtResult::NXT_ERROR;
	}
	pRenderer->swapChain = static_cast<IDXGISwapChain3*>(swapChain1);

	RenderTarget* primaryRtv = new RenderTarget();
	pRenderer->rtvDescriptorOffset = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	for (int i = 0; i < rendererDesc.bufferCount; i++) {
		if (FAILED(pRenderer->swapChain->GetBuffer(i, IID_PPV_ARGS(&pRenderer->primaryRtvTextures[i])))) {
			Log::AppendMessage(LogLevel::LOG_ERROR, "1004");
			return NxtResult::NXT_ERROR;
		}

		D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pRenderer->srcRtvHeap->GetCPUDescriptorHandleForHeapStart();
		rtvHandle.ptr = rtvHandle.ptr + pRenderer->rtvDescriptorOffset * i;

		device->CreateRenderTargetView(pRenderer->primaryRtvTextures[i], nullptr, rtvHandle);
		primaryRtv->rtvHeapIndex[i] = i;
		primaryRtv->texture[i] = new Texture();
		primaryRtv->texture[i]->pResource = pRenderer->primaryRtvTextures[i];

		primaryRtv->texture[i]->currState = ResourceState::PRESENT;
		primaryRtv->texture[i]->dstState = ResourceState::RENDER_TARGET;
	}
	pRenderer->rtvHandlesAllocated = 3;
	pRenderer->dsvHandlesAllocated = 0;

	LoadOperations loadOperations[] = {
		LoadOperations::CLEAR
	};

	pRenderer->primaryRenderPass = new RenderPass();
	pRenderer->primaryRenderPass->renderTargetCount = 1;
	pRenderer->primaryRenderPass->subpassCount = 1;
	pRenderer->primaryRenderPass->loadOperations[0] = LoadOperations::CLEAR;
	pRenderer->primaryRenderPass->pRenderTargets[0] = primaryRtv;
	memcpy(pRenderer->primaryRenderPass->clearColor, rendererDesc.clearColor, sizeof(float) * 4);

	return NxtResult::NXT_OK;
}

NxtResult nxt::CreateCommandBuffer(CommandBuffer** cmdBuffer) {

	*cmdBuffer = new CommandBuffer();
	ID3D12Device* device = pRenderer->device;
	for (int i = 0; i < pRenderer->bufferCount; i++) {

		if (FAILED(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&(*cmdBuffer)->cmdAllocator[i])))) {
			Log::AppendMessage(LogLevel::LOG_ERROR, "2000");
			return NxtResult::NXT_ERROR;
		}

		(*cmdBuffer)->fenceValue[i] = 0;
		if (FAILED(device->CreateFence((*cmdBuffer)->fenceValue[i], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&(*cmdBuffer)->fence[i])))) {
			Log::AppendMessage(LogLevel::LOG_ERROR, "2001");
			return NxtResult::NXT_ERROR;
		}
	}
	
	if (FAILED(device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, (*cmdBuffer)->cmdAllocator[0], nullptr, IID_PPV_ARGS(&(*cmdBuffer)->cmdList)))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "2002");
		return NxtResult::NXT_ERROR;
	}

	(*cmdBuffer)->cmdList->Close();
	(*cmdBuffer)->fenceEvent = CreateEvent(nullptr, false, false, L"");

	return NxtResult::NXT_OK;
}

NxtResult nxt::BeginRecording(CommandBuffer* cmdBuffer) {

	ID3D12GraphicsCommandList* cmdList = cmdBuffer->cmdList;
	ID3D12CommandAllocator** cmdAlloc = cmdBuffer->cmdAllocator;
	ID3D12Fence** fence = cmdBuffer->fence;
	unsigned int frameIndex = pRenderer->swapChain->GetCurrentBackBufferIndex();

	unsigned long long* fenceValues = cmdBuffer->fenceValue;
	if (fence[frameIndex]->GetCompletedValue() < fenceValues[frameIndex]) {
		if (FAILED(fence[frameIndex]->SetEventOnCompletion(fenceValues[frameIndex], cmdBuffer->fenceEvent))) {
			Log::AppendMessage(LogLevel::LOG_ERROR, "3000");
			return NxtResult::NXT_ERROR;
		}
		WaitForSingleObject(cmdBuffer->fenceEvent, INFINITE);
	}

	if (FAILED(cmdAlloc[frameIndex]->Reset())) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "3002");
		return NxtResult::NXT_ERROR;
	}

	if (FAILED(cmdList->Reset(cmdAlloc[frameIndex], nullptr))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "3001");
		return NxtResult::NXT_ERROR;
	}

	return NxtResult::NXT_OK;
}

NxtResult nxt::CreateRenderPass(RenderPassDesc renderPassDesc, RenderPass** renderPass) {

	RenderTarget* rtv[8];
	memset(rtv, 0, sizeof(RenderTarget*) * 8);

	RenderTarget* depthTarget = nullptr;
	LoadOperations loadOperations[8];
	ResourceState srcStates[8];
	ResourceState dstStates[8];

	int renderTargetIndex = 0;
	std::unordered_map<std::string, RenderTarget*> rt;
	for (int i = 0; i < renderPassDesc.attachmentCount; i++) {
		for (int j = 0; j < 3; j++) {
			TextureDesc textureDesc{};
			textureDesc.width = renderPassDesc.rtDesc[i].width;
			textureDesc.height = renderPassDesc.rtDesc[i].height;
			textureDesc.depthOrArraySize = 1;
			textureDesc.format = (!renderPassDesc.rtDesc[i].depth) ? renderPassDesc.rtDesc[i].colorFormat : renderPassDesc.rtDesc[i].depthFormat;
			textureDesc.sampleCount = 1;
			textureDesc.sampleQuality = 0;
			textureDesc.flags = (renderPassDesc.rtDesc[i].depth) ? D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL : D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

			D3D12_CLEAR_VALUE clearValue{};
			if (!renderPassDesc.rtDesc[i].depth) {
				clearValue.Color[0] = renderPassDesc.clearColor[0];
				clearValue.Color[1] = renderPassDesc.clearColor[1];
				clearValue.Color[2] = renderPassDesc.clearColor[2];
				clearValue.Color[3] = renderPassDesc.clearColor[3];
				clearValue.Format = ConvertFormat(renderPassDesc.rtDesc[i].colorFormat);
			}
			else {
				clearValue.Format = ConvertFormat(renderPassDesc.rtDesc[i].depthFormat);
				clearValue.DepthStencil.Depth = 1.0f;
				clearValue.DepthStencil.Stencil = 0;
			}

			Texture* texture = CreateTexture(textureDesc, clearValue, false);
			if (renderPassDesc.rtDesc[i].depth) {
				D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc{};
				dsvDesc.Format = ConvertFormat(renderPassDesc.rtDesc[i].depthFormat);
				dsvDesc.Texture2D.MipSlice = 0;
				dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;

				D3D12_CPU_DESCRIPTOR_HANDLE dsvHandle = pRenderer->srcDsvHeap->GetCPUDescriptorHandleForHeapStart();
				dsvHandle.ptr = dsvHandle.ptr + pRenderer->dsvHandlesAllocated * pRenderer->device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);

				if (!depthTarget)
					depthTarget = new RenderTarget();

				depthTarget->name = renderPassDesc.rtDesc[i].name;
				depthTarget->texture[j] = texture;
				depthTarget->rtvHeapIndex[j] = pRenderer->dsvHandlesAllocated;
				pRenderer->device->CreateDepthStencilView(depthTarget->texture[j]->pResource, &dsvDesc, dsvHandle);
				pRenderer->dsvHandlesAllocated++;

				rt[depthTarget->name] = depthTarget;

			}
			else {
				D3D12_RENDER_TARGET_VIEW_DESC rtvDesc{};
				rtvDesc.Texture2D.MipSlice = 0;
				rtvDesc.Format = ConvertFormat(renderPassDesc.rtDesc[i].colorFormat);
				rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;

				D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pRenderer->srcRtvHeap->GetCPUDescriptorHandleForHeapStart();
				rtvHandle.ptr = rtvHandle.ptr + pRenderer->rtvHandlesAllocated * pRenderer->rtvDescriptorOffset;

				if (!rtv[renderTargetIndex])
					rtv[renderTargetIndex] = new RenderTarget();

				rtv[renderTargetIndex]->name = renderPassDesc.rtDesc[i].name;
				rtv[renderTargetIndex]->texture[j] = texture;
				rtv[renderTargetIndex]->rtvHeapIndex[j] = pRenderer->rtvHandlesAllocated;

				rt[rtv[renderTargetIndex]->name] = rtv[renderTargetIndex];

				pRenderer->device->CreateRenderTargetView(rtv[renderTargetIndex]->texture[j]->pResource, &rtvDesc, rtvHandle);
				pRenderer->rtvHandlesAllocated++;
			}
		}

		loadOperations[i] = renderPassDesc.loadOps[i];
		if (!renderPassDesc.rtDesc[i].depth)
			renderTargetIndex++;
	}

	*renderPass = new RenderPass();
	(*renderPass)->renderTargetCount = renderPassDesc.attachmentCount;
	(*renderPass)->pDepthTarget = depthTarget;
	memcpy((*renderPass)->loadOperations, loadOperations, sizeof(LoadOperations)*renderPassDesc.attachmentCount);
	memcpy((*renderPass)->pRenderTargets, rtv, sizeof(RenderTarget*) * renderTargetIndex);
	memcpy((*renderPass)->clearColor, renderPassDesc.clearColor, sizeof(float) * 4);

	return NxtResult::NXT_OK;
}

NxtResult nxt::CreateVertexBuffer(VertexBuffer** vertexBuffer, uint32_t stride, uint32_t byteWidth) {
	
	(*vertexBuffer) = new VertexBuffer();
	(*vertexBuffer)->buffer = CreateBuffer(byteWidth);

	(*vertexBuffer)->vbView.BufferLocation = (*vertexBuffer)->buffer->pResource->GetGPUVirtualAddress();
	(*vertexBuffer)->vbView.StrideInBytes = stride;
	(*vertexBuffer)->vbView.SizeInBytes = byteWidth;

	return NxtResult::NXT_OK;
}

NxtResult nxt::CmdCopyBufferData(CommandBuffer* cmdBuffer, VertexBuffer* vb, void* pData, uint32_t stride, uint32_t byteWidth) {

	ID3D12GraphicsCommandList* cmdList = cmdBuffer->cmdList;
	D3D12_SUBRESOURCE_DATA resourceData{};
	resourceData.pData = pData;
	resourceData.SlicePitch = byteWidth;
	resourceData.RowPitch = byteWidth;

	UpdateSubresources(cmdList, vb->buffer->pResource, vb->buffer->pResourceUpload, 0, 0, 1, &resourceData);
	
	D3D12_RESOURCE_BARRIER resBarrier{};
	resBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
	cmdList->ResourceBarrier(1, &resBarrier);

	return NxtResult::NXT_OK;
}

NxtResult nxt::CmdBeginRenderPass(CommandBuffer* cmdBuffer, RenderPass* renderPass) {

	ID3D12GraphicsCommandList* cmdList = cmdBuffer->cmdList;
	unsigned int currentBuffer = pRenderer->swapChain->GetCurrentBackBufferIndex();

	D3D12_RESOURCE_BARRIER resBarrier[8];
	for (int i = 0; i < renderPass->renderTargetCount; i++) {
		int rtvHeapIndex = renderPass->pRenderTargets[i]->rtvHeapIndex[currentBuffer];
		CopyDescriptor(rtvHeapIndex, i, pRenderer->srcRtvHeap, pRenderer->dstRtvHeap, D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		D3D12_RESOURCE_BARRIER resourceBarrier{};
		resourceBarrier.Transition.StateBefore = ConvertNxtResourceState(renderPass->pRenderTargets[i]->texture[currentBuffer]->currState);
		resourceBarrier.Transition.StateAfter = ConvertNxtResourceState(renderPass->pRenderTargets[i]->texture[currentBuffer]->dstState);
		resourceBarrier.Transition.pResource = renderPass->pRenderTargets[i]->texture[currentBuffer]->pResource;

		resBarrier[i] = resourceBarrier;
	}
	cmdList->ResourceBarrier(renderPass->renderTargetCount, resBarrier);

	D3D12_CPU_DESCRIPTOR_HANDLE handle = pRenderer->dstRtvHeap->GetCPUDescriptorHandleForHeapStart();
	cmdList->OMSetRenderTargets(renderPass->renderTargetCount, &handle, true, nullptr);
	for (int i = 0; i < renderPass->renderTargetCount; i++) {
		D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pRenderer->dstRtvHeap->GetCPUDescriptorHandleForHeapStart();
		rtvHandle.ptr = rtvHandle.ptr + i * pRenderer->device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		cmdList->ClearRenderTargetView(rtvHandle, renderPass->clearColor, 0, nullptr);
	}

	return NxtResult::NXT_OK;
}

NxtResult nxt::CmdEndRenderPass(CommandBuffer* cmdBuffer, RenderPass* renderPass) {

	ID3D12GraphicsCommandList* cmdList = cmdBuffer->cmdList;
	unsigned int currentBuffer = pRenderer->swapChain->GetCurrentBackBufferIndex();

	D3D12_RESOURCE_BARRIER resBarrier[8];
	for (int i = 0; i < renderPass->renderTargetCount; i++) {
		D3D12_RESOURCE_BARRIER resourceBarrier{};
		resourceBarrier.Transition.StateBefore = ConvertNxtResourceState(renderPass->pRenderTargets[i]->texture[currentBuffer]->currState);
		resourceBarrier.Transition.StateAfter = ConvertNxtResourceState(renderPass->pRenderTargets[i]->texture[currentBuffer]->dstState);
		resourceBarrier.Transition.pResource = renderPass->pRenderTargets[i]->texture[currentBuffer]->pResource;

		resBarrier[i] = resourceBarrier;
	}
	cmdList->ResourceBarrier(renderPass->renderTargetCount, resBarrier);

	return NxtResult::NXT_OK;
}

NxtResult nxt::EndRecording(CommandBuffer* cmdBuffer) {
	if (FAILED(cmdBuffer->cmdList->Close())) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "3003");
		return NxtResult::NXT_ERROR;
	}

	return NxtResult::NXT_OK;
}

NxtResult nxt::SubmitCommandBuffers(uint8_t numCommandBuffers, CommandBuffer** cmdBuffer) {

	ID3D12CommandList* cmdLists[16];
	unsigned int frameIndex = pRenderer->swapChain->GetCurrentBackBufferIndex();

	for (int i = 0; i < numCommandBuffers; i++) {
		cmdLists[i] = cmdBuffer[i]->cmdList;
		cmdBuffer[i]->fenceValue[frameIndex]++;
	}

	pRenderer->primaryCmdQueue->ExecuteCommandLists(numCommandBuffers, cmdLists);
	for (int i = 0; i < numCommandBuffers; i++) {
		if (FAILED(pRenderer->primaryCmdQueue->Signal(cmdBuffer[i]->fence[frameIndex], cmdBuffer[i]->fenceValue[frameIndex]))) {
			Log::AppendMessage(LogLevel::LOG_ERROR, "4000");
			return NxtResult::NXT_ERROR;
		}
	}
	
	if (FAILED(pRenderer->swapChain->Present(0, 0))) {
		Log::AppendMessage(LogLevel::LOG_ERROR, "4001");
		return NxtResult::NXT_ERROR;
	}

	return NxtResult::NXT_OK;
}
#endif