#pragma once
#include <d3d12.h>
#include "../RenderTargetManager.h"

namespace nxt {

	DXGI_FORMAT ConvertFormat(nxt::Format);
	Texture* CreateTexture(TextureDesc textureDesc, bool depth, bool upload);
	struct RenderTarget {
		std::string name;
		int rtvHeapIndex[3];
		Texture* texture[3];
		bool depth;
	};

	class D3D12RenderTargetManager : public IRenderTargetManager {
	public:
		virtual IResource* Load(RenderTargetDesc renderTargetDesc) {

			RenderTarget* renderTarget = new RenderTarget();
			for (int j = 0; j < 3; j++) {
				TextureDesc textureDesc{};
				textureDesc.width = renderTargetDesc.width;
				textureDesc.height = renderTargetDesc.height;
				textureDesc.depthOrArraySize = 1;
				textureDesc.format = (!renderTargetDesc.depth) ? renderTargetDesc.colorFormat : renderTargetDesc.depthFormat;
				textureDesc.sampleCount = 1;
				textureDesc.sampleQuality = 0;
				textureDesc.flags = (renderTargetDesc.depth) ? D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL : D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

				D3D12_CLEAR_VALUE clearValue{};
				if (!renderTargetDesc.depth) {
					clearValue.Color[0] = renderTargetDesc.clearColor[0];
					clearValue.Color[1] = renderTargetDesc.clearColor[1];
					clearValue.Color[2] = renderTargetDesc.clearColor[2];
					clearValue.Color[3] = renderTargetDesc.clearColor[3];
					clearValue.Format = ConvertFormat(renderTargetDesc.colorFormat);
				}
				else {
					clearValue.Format = ConvertFormat(renderTargetDesc.depthFormat);
					clearValue.DepthStencil.Depth = 1.0f;
					clearValue.DepthStencil.Stencil = 0;
				}

				Texture* texture = CreateTexture(textureDesc, clearValue, false);
				if (renderTargetDesc.depth) {
					D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc{};
					dsvDesc.Format = ConvertFormat(renderTargetDesc.depthFormat);
					dsvDesc.Texture2D.MipSlice = 0;
					dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;

					D3D12_CPU_DESCRIPTOR_HANDLE dsvHandle = pRenderer->srcDsvHeap->GetCPUDescriptorHandleForHeapStart();
					dsvHandle.ptr = dsvHandle.ptr + pRenderer->dsvHandlesAllocated * pRenderer->device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);

					if (!depthTarget)
						depthTarget = new RenderTarget();

					depthTarget->name = renderTargetDesc.name;
					depthTarget->texture[j] = texture;
					depthTarget->rtvHeapIndex[j] = pRenderer->dsvHandlesAllocated;
					pRenderer->device->CreateDepthStencilView(depthTarget->texture[j]->pResource, &dsvDesc, dsvHandle);
					pRenderer->dsvHandlesAllocated++;
				}
				else {
					D3D12_RENDER_TARGET_VIEW_DESC rtvDesc{};
					rtvDesc.Texture2D.MipSlice = 0;
					rtvDesc.Format = ConvertFormat(renderPassDesc.rtDesc[i].colorFormat);
					rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;

					D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pRenderer->srcRtvHeap->GetCPUDescriptorHandleForHeapStart();
					rtvHandle.ptr = rtvHandle.ptr + pRenderer->rtvHandlesAllocated * pRenderer->rtvDescriptorOffset;

					if (!rtv[renderTargetIndex])
						rtv[renderTargetIndex] = new RenderTarget();

					rtv[renderTargetIndex]->name = renderPassDesc.rtDesc[i].name;
					rtv[renderTargetIndex]->texture[j] = texture;
					rtv[renderTargetIndex]->rtvHeapIndex[j] = pRenderer->rtvHandlesAllocated;

					pRenderer->device->CreateRenderTargetView(rtv[renderTargetIndex]->texture[j]->pResource, &rtvDesc, rtvHandle);
					pRenderer->rtvHandlesAllocated++;
				}
			}
			return renderTarget;
		}
	};
};