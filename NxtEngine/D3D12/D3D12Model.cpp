#include "../../IModel.h"

nxt::Model* nxt::CreateModel(CommandBuffer* cmdBuffer, VertexType vertexType, ImporterType importerType, ModelTypes modelTypes, const char* modelName) {

	Model* model = new Model();
	if (importerType == ImporterType::NONE) {
		if (modelTypes == ModelTypes::TRIANGLE) {
			StandardVertex vertices[] = {
				{-0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f},
				{ 0.0f,  0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f},
				{ 0.5f, -0.5f, 0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f}
			};

			CreateVertexBuffer(&model->vb, sizeof(StandardVertex), sizeof(StandardVertex) * 3);
			CmdCopyBufferData(cmdBuffer, model->vb, vertices, sizeof(StandardVertex), sizeof(StandardVertex) * 3);
			model->numberOfSubset = 0;
		}
	}
	return model;
}
