#pragma once
#include "GraphicsTypes.h"
#include "ResourceManager.h"
#include "IRenderer.h"

namespace nxt {
	struct RenderTargetDesc {
		std::string name;
		uint32_t width, height;
		ResourceState srcStates;
		ResourceState dstStates;

		Format colorFormat;
		Format depthFormat;
		int    depth;
		float  clearColor[4];
	};

	class IRenderTargetManager
	{
	public:
		virtual IResource* Load(RenderTargetDesc renderTargetDesc) = 0;
	};
};

