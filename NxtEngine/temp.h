#pragma once


/*

{
  "Resources": [
    {
      "Name": "ZPrepassRT",
      "ResourceType": "RenderTarget",
      "Pass": 0,
      "Format": "D32",
      "Width": 1920,
      "Height": 1080
    },
    {
      "Name": "ForwardRT",
      "ResourceType": "RenderTarget",
      "Pass": 1,
      "Format": "R8G8B8A8_UNORM",
      "Width":  1920,
      "Height": 1080
     }
  ],
  "numberOfPasses": 1,
  "passes": [
    {
      "Name": "ForwardPass",
      "Subpass": 0,
      "SubpassCount": 2,
      "Subpasses": [
        {
          "SubpassName": "ZPrepass",
          "PassPipeline": true,
          "Pipeline": {
            "VertexShader": "DepthWriteVert.hlsl",
            "PixelShader": "DepthWriteFrag.hlsl",
            "RenderTargetCount": 0,
            "Format": [ "D32" ],
            "InputLayout": "VERTEX_POS_TEX_NORM_TANG_BITANG",
            "Depth": "Default",
            "Blend": "Default",
            "Rasterizer": "Default"
          },
          "Input": {
            "Resources": [
              {
                "Name": "ZPrepassRT",
                "ResourceState": "DepthWrite"
              }
            ]
          }
        },
        {
          "SubpassName": "FowardRender",
          "PassPipeline": false,
          "Pipeline": {
            "Depth": "Read",
          },
          "Input": {
            "Resources": [
              {
                "Name": "ForwardRT",
                "ResourceState": "RenderTarget"
              },
              {
                "Name": "ZPrepassRT",
                "ResourceState": "DepthRead"
              }
            ]
          }
        },
        {
          "SubpassName": "Present",
          "PassPipeline": true,
          "PassRender": true,
          "Pipeline": {
            "Depth": "Read"
          },
          "Input": {
            "Resources": [
              {
                "Name": "ForwardRT",
                "ResourceState": "Present"
              }
            ]
          }
        }
      ],
      "Resources": [
        {
          "Name": "ZPrepassRT",
          "ResourceState": "DepthWrite"
        },
        {
          "Name": "ForwardRT",
          "ResourceState": "RenderTarget"
        }
      ],
      "Output": [
        {
          "DestinationPassIndex": 1,
          "DestinationPassName": "ForwardPass",
          "ResourceName": "ZPrepassRT",
          "DestinationResourceState": "DepthRead"
        }
      ],
      "Input": [],
      "PassPipeline": true,
      "Pipeline": {
        "VertexShader": "DepthWriteVert.hlsl",
        "PixelShader": "DepthWriteFrag.hlsl",
        "RenderTargetCount": 0,
        "Format": [ "D32" ],
        "InputLayout": "VERTEX_POS_TEX_NORM_TANG_BITANG",
        "Depth": "Default",
        "Blend": "Default",
        "Rasterizer": "Default"
      }
    },
    {
      "SubpassCount": 1,
      "PassPipeline": false,
      "Subpasses": [
        {
          "Subpass": 1,
          "Name": "ForwardPass",
          "Resources": [
            {
              "Name": "ForwardRT",
              "ResourceState": "Present"
            }
          ],
          "Output": [],
          "PassPipeline": true,
          "PassRender": true,
          "Pipeline": {
            "VertexShader": "PresentVert.hlsl",
            "PixelShader": "PresentFrag.hlsl",
            "RenderTargetCount": 1,
            "Format": [ "R8G8B8A8_UNORM" ],
            "InputLayout": "VERTEX_POS_TEX",
            "Depth": "Default",
            "Blend": "Default",
            "Rasterizer": "Default"
          }
        }
      ],
      "Pipeline": {
        "Depth": "DepthRead"
      },
      "Resources": [
        {
          "Name": "ForwardRT",
          "ResourceState": "RenderTarget"
        }
      ],
      "Name": "ForwardPass",
      "Output": [
      ],
      "Input": [
        {
          "Name": "ZPrepass",
          "ResourceName": "ZPrepassRT",
          "DestinationResourceState": "DepthRead"
        }
      ]
    }
  ]
}

*/