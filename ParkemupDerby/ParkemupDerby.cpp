#include "../NxtEngine/IRenderer.h"
#include "../NxtEngine/IModel.h"
#include "../NxtEngine/IOperatingSystem.h"
#include "../NxtEngine/RenderGraph.h"

using namespace nxt;

Model* gQuad;
CommandBuffer* gCmdBuffer;
RenderPass* gRenderPass;

void mainloop() {

	BeginRecording(gCmdBuffer);

	CmdBeginRenderPass(gCmdBuffer, gRenderPass);
//	CmdDrawModel(gCmdBuffer, gQuad);
//	CmdNextSubpass();

	//CmdDrawModel(gCmdBuffer, gQuad);
	CmdEndRenderPass(gCmdBuffer, gRenderPass);

	EndRecording(gCmdBuffer);

	CommandBuffer* cmdBuffers[] = { gCmdBuffer };
	SubmitCommandBuffers(1, cmdBuffers);
}

int main() {

	WindowDesc windowDesc{};
	windowDesc.height = 600;
	windowDesc.width = 800;
	windowDesc.windowName = "WindowClass";

	WindowHandle* windowHandle = InitWindow(windowDesc);

	RendererDesc rendererDesc{};
	rendererDesc.bufferCount = 3;
	rendererDesc.displayScale = 1.0f;
	rendererDesc.height = 600;
	rendererDesc.width = 800;
	rendererDesc.refreshRate = 60;
	rendererDesc.window = 1;
	rendererDesc.pWindowHandle = windowHandle;
	rendererDesc.clearColor[0] = 0.2f;
	rendererDesc.clearColor[1] = 0.4f;
	rendererDesc.clearColor[2] = 0.6f;
	rendererDesc.clearColor[3] = 1.0f;

	InitializeRenderer(rendererDesc);
	CreateCommandBuffer(&gCmdBuffer);

	gRenderPass = GetPrimaryRenderPass();
	//gQuad       = CreateModel(gCmdBuffer, VertexType::VERTEX_POS_TEX_NORM, ImporterType::NONE, ModelTypes::QUAD, "");

	RenderGraph* renderGraph = new RenderGraph("");

	MainLoop(windowHandle, mainloop);

	return 0;
}